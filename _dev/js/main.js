var Utils = {
    "digitsInString": function (str) {
        return str.replace(/\D+/g, '');
    },
    "bitNumber": function (digit, additionalChar) {
        additionalChar = additionalChar || "&nbsp;";

        return digit.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1' + additionalChar);
    }
};


function openCard() {
    var card = $('.card__data');

    card.on('click', function () {
        var header = $(this).parent('.card__header');
        var body = $(header).siblings('.card__body');

        if($(header).hasClass('is-open')){
            $(header).removeClass('is-open');
            body.slideUp();
        }
        else{
            $('.card__header').removeClass('is-open');
            $('.card__body').slideUp();
            $(header).addClass('is-open');
            body.slideDown();
        }

    })
}

function viewFilter() {
    var tab = $('.js-tab'),
        card = $('.card');

    tab.on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('is-active')) {
            return false
        }
        else {
            tab.removeClass('is-active');
            $(this).addClass('is-active');
            var id = $(this).attr('data-href');
            choiseCard(card, id);
        }
    })
}

function choiseCard(card, id) {
    var count = $('.card').length;
    card.addClass('is-hide');

    if (id == '#all') {
        card.removeClass('is-hide');
    }
    else {
        for (var i = 0; i < count; i++) {
            var cur_id = $(card[i]).attr('data-id');

            if (cur_id == id) {
                $('[data-id="' + cur_id + '"]').removeClass('is-hide');
            }
        }
    }
}

function viewTab() {
    var tab = $('.js-tab-link'),
        slide = $('.slider-slide'),
        wrap = $('#swiper-tab');

    selectTab(tab, slide, wrap);
}

function selectTab(tab, slide, wrap) {
    tab.on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('is-active')) {
            return false;
        }

        else {
            tab.removeClass('is-active');
            var id = $(this).attr('href');
            slide.removeClass('is-view');
            $(id).addClass('is-view');

            var cur_id = $(wrap).find('[href="' + id + '"]');
            $(cur_id).addClass('is-active');
        }
    })
}

function viewAboutTab() {
    var tab = $('.js-about-tab'),
        slide = $('.about__slide'),
        wrap = $('.about__tab');

    selectTab(tab, slide, wrap);
}

function initSwiper() {

    if ($(window).width() < 768) {
        var mySwiper = new Swiper('#swiper-tab', {
            slidesPerView: 4.5,
            setWrapperSize: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },

            breakpoints: {
                360: {
                    slidesPerView: 1.75
                },
                480: {
                    slidesPerView: 2.5
                },
                640: {
                    slidesPerView: 3.5
                }
            }
        });
    }

    if ($(window).width() < 768) {
        var mySwiper = new Swiper('#price-tab', {
            slidesPerView: 3.75,
            setWrapperSize: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },

            breakpoints: {
                380: {
                    slidesPerView: 1.75
                },
                480: {
                    slidesPerView: 2.5
                },
                640: {
                    slidesPerView: 2.75
                }
            }
        });
    }
}

function changeTotalSum() {
    $('[type="radio"]').change(function () {
        var card = $(this).parents('.card');
        var total = $(card).find('.js-cost');
        var totalCost = $(total).attr('data-sum');
        var transfer = $(this).attr('data-sum');
        var t = parseInt(transfer);
        var s = parseInt(totalCost);

        var totalSum = s + t;

        $(total).text(bitNumber(totalSum));
    })
}

function initMagnificPopup(params) {

    var selector = $(params.selector);

    if (selector.length) {
        selector.on('click', function (e) {
            e.stopPropagation();

            $(this).magnificPopup({
                type: 'inline'
            }).magnificPopup('open');

            e.preventDefault();
        });
    }

    var mfpClose = $('.js-mfp-close');

    mfpClose.on('click', function () {
        $.magnificPopup.close();
    })
}

function bitNumber(digit, additionalChar) {
    additionalChar = additionalChar || " ";

    return digit.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1' + additionalChar);
}

function sendForm(form, url, booking) {
    var data = form.serialize();

    $.ajax({
        method: "POST",
        url: 'http://smena.roscamps.ru/' + url,
        data: data
    }).done(function (msg) {
        var valdata = JSON.parse(msg);

        if (!valdata.valid) {
            alert('Пожалуйста убедитесь, что все поля формы заполнены правильно.')
        } else {
            $.magnificPopup.close();
            showSendSuccessMes(form.data('type'));
            booking.formReset();
            //B!!!!
            form[0].reset();
            reachGoal(url);
        }
    });
}

//для отслеживания события в GTM
function reachGoal(url){
    if(url == 'valid_question.php'){
        dataLayer.push({'event': 'callback-form'});
    }
    else if(url == 'valid_callback.php'){
        dataLayer.push({'event': 'question-form'});
    }

    else{
        dataLayer.push({'event': 'booking-form'});
    }

}

function showSendSuccessMes(type) {
    if (!type) {
        throw new Error('message type undefined');
    }

    var mes = $('#mfp-success');

    var mesTxt = {
        "question": {
            "title": "Спасибо за&nbsp;обращение!",
            "txt": "Ваше обращение отправлено! Наши специалисты свяжутся с&nbsp;Вами в&nbsp;ближайшее время"
        },
        "request": {
            "title": "Ваша заявка успешно отправлена!",
            "txt": "В&nbsp;ближайшее время с&nbsp;Вами свяжутся наши&nbsp;специалисты для&nbsp;уточнения деталей заказа"
        }
    };

    mes.find('.popup__title').html(mesTxt[type].title);
    mes.find('.popup__success').html(mesTxt[type].txt);

    $.magnificPopup.open({
        items: {
            src: mes
        },
        type: 'inline'
    });
}

function bookingPopup() {

    var getOfferBtn = $('.js-get-offer');

    var bookingForm = $('#form-booking');

    var offerReplacmentData = {
        "pic": $('#offer-pic'),
        "title": $('#offer-title'),
        "period": $('#offer-periods'),
        "cost": $('#offer-cost'),
        "lname": $('#lname'),
        "lperiod": $('#lperiod'),
        "transfer": $('.radio')
    };

    var booking = new BookingFormEvents(bookingForm, offerReplacmentData);

    function BookingFormEvents(form, plhs) {
        if (!form.length) {
            throw new Error('booking form not found');
        }

        var fn = this;

        var childsNum = $('#child-num');
        var defCost;
        var radioBtn = $('#mfp-booking [type="radio"]');


        childsNum.on('change', function (e) {
            var el = $('#mfp-booking [type="radio"]:checked');
            transferCalc(el);
        });

        radioBtn.on('change', function (e) {
            transferCalc($(this));
        });

        function childsCalc(field) {
            var val = field.val();

            if (val) {
                defCost = Utils.digitsInString(plhs.cost.data('def-cost'));
                setTotalCost(plhs.cost, calcTotal(val, defCost));
            }
        }

        function transferCalc(field) {
            var val = childsNum.val(); //кол-во детей
            var addSum = $(field).attr('data-sum');

            if (val) {
                defCost = Utils.digitsInString(plhs.cost.data('vCost'));
                var t = parseInt(addSum);
                var s = parseInt(defCost);

                defCost = (t + s) * val;

                setTotalCost(plhs.cost, defCost);
            }
        }

        function calcTotal(num, cost) {
            return Utils.bitNumber(cost * num);
        }

        function setTotalCost(elCost, cost) {
            elCost.html(Utils.bitNumber(cost) + ' руб.');
        }

        fn.formReset = function () {
            form[0].reset();
        };
    }


    function getCamp(el) {
        return el.parents('.card');
    }

    getOfferBtn.on('click', function () {
        prepareOfferPopup(getCamp($(this)), offerReplacmentData, booking);
    });

    function prepareOfferPopup(camp, plhs, booking) {

        var plhData = collectData(camp);

        function collectData(el) {

            return {
                "pic": el.find('.card__img').children('img').attr('src'),
                "title": el.find('.card__title').text(),
                "cost": el.find('.card__cost').text(),
                "vCost": el.find('.js-cost').attr('data-sum'),
                "period": el.attr('data-period'),
                "transfer": el.find('input[type="radio"]:checked').val()
            }
        }

        function setData(plhs, data) {
            plhs.pic.attr('src', data.pic);
            plhs.title.text(data.title);
            plhs.lname.attr('value', data.title);
            plhs.lperiod.attr('value', data.period);

            plhs.cost.html(data.cost);
            plhs.cost.data('def-cost', data.cost);
            plhs.cost.data('vCost', data.vCost);
            plhs.period.text(data.period);

            var v = data.transfer;
            var t = $('.js-radio').filter('[value="' + v + '"]');
            t.prop('checked', true);
        }

        setData(plhs, plhData);

        $.magnificPopup.open({
            items: {
                src: '#mfp-booking'
            },
            type: 'inline',
            callbacks: {
                close: function () {
                    booking.formReset();
                }
            }
        });
    }


    var mainForm = $('#main-form');
    var btn = $(mainForm).find('button');

    $('#lager').on('change', function () {
        var val = $('#lager option:selected').attr('data-sum');
        if (val.length) {
            val = Utils.bitNumber(val);
            $(mainForm).find('.js-cost').html(val);
        }
    });

    $(btn).on('click', function (e) {
        e.preventDefault();
        if ($('#smena option:selected').val() == "") {
            alert('Выберите смену');
            return
        }
        if ($('#lager option:selected').val() == "") {
            alert('Выберите направление');
            return
        }

        else {
            offerPopup(mainForm, offerReplacmentData, booking);
        }
    });

    function offerPopup(camp, plhs, booking) {

        var plhData = collectData(camp);

        function collectData(el) {

            return {
                "pic": el.find('#lager option:selected').attr('data-src'),
                "title": el.find('#lager option:selected').val(),
                "cost": el.find('.js-cost').text(),
                "vCost": el.find('.js-cost').text(),
                "period": el.find('#smena option:selected').val()
            }
        }

        function setData(plhs, data) {
            plhs.pic.attr('src', data.pic);
            plhs.title.text(data.title);
            plhs.lname.attr('value', data.title);
            plhs.lperiod.attr('value', data.period);

            plhs.cost.html(data.cost);
            plhs.cost.data('def-cost', data.cost);
            plhs.cost.data('vCost', data.vCost);
            plhs.period.text(data.period);
        }

        setData(plhs, plhData);

        $.magnificPopup.open({
            items: {
                src: '#mfp-booking'
            },
            type: 'inline',
            callbacks: {
                close: function () {
                    booking.formReset();
                }
            }
        });
    }


    $.validate({
        form: '#form-booking',
        lang: 'ru',
        showHelpOnFocus: false,
        addSuggestions: false,
        scrollToTopOnError: false,
        onError: function ($form) {
            return false;
        },
        onSuccess: function ($form) {
            sendForm($form, 'valid_request.php', booking);
            return false;
        }
    });

    $.validate({
        form: '#pop-contacts',
        lang: 'ru',
        showHelpOnFocus: false,
        addSuggestions: false,
        scrollToTopOnError: false,
        onError: function ($form) {
            return false;
        },
        onSuccess: function ($form) {
            sendForm($form, 'valid_question.php', booking);
            return false;
        }
    });

    $.validate({
        form: '#q-form',
        lang: 'ru',
        showHelpOnFocus: false,
        addSuggestions: false,
        scrollToTopOnError: false,
        onError: function ($form) {
            return false;
        },
        onSuccess: function ($form) {
            sendForm($form, 'valid_callback.php', booking);
            return false;
        }
    });
}

function placesWrite(){
    $.ajax({
        method: "POST",
        url: 'http://smena.roscamps.ru/get_data.php',
        success: function(data) {
            var place = JSON.parse(data);
            var placeLeft = place.places_left;
            $('.js-place').text(placeLeft);
        },
        error: function(){
            console.log('некорректные данные оставшихся мест');
        }
    });
}


$(document).ready(function () {
    $('select').styler();
    viewTab();
    openCard();
    viewFilter();
    initSwiper();
    viewAboutTab();
    changeTotalSum();
    initMagnificPopup({'selector': '.js-mfp'});
    bookingPopup();
    placesWrite();
});